# Simple TCP implementation

Files written during the Computer Networks course at the University of Padua, Computer Engineering.

## Single Files

- *arp.c*: Implementation of the ARP protocol
- *bellman.c* and *dijkstra*: Implementation of the Dijkstra and Bellman-Ford algorithms  
- *ping.c*: Implementation of the ping command
- *congctrl.c*: Basic implementation of the TCP and IP protocol with congestion control
