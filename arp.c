#include <net/if.h>
#include <strings.h>
#include <arpa/inet.h>
#include<stdlib.h>
#include <sys/types.h>
#include<stdio.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
unsigned char myip[4] = { 88,80,187,84 };
unsigned char mymac[6] = {0xf2,0x3c,0x91,0xdb,0xc2,0x98};
unsigned char broadcastmac[6] = {0xff,0xff,0xff,0xff,0xff,0xff};

struct arp_packet {
unsigned short int htype;
unsigned short int ptype;
unsigned char hlen;
unsigned char plen;
unsigned short op;
unsigned char srcmac[6];
unsigned char srcip[4];
unsigned char dstmac[6];
unsigned char dstip[4];
};

struct ethernet_frame {
unsigned char dstmac[6];
unsigned char srcmac[6];
unsigned short int type;
unsigned char payload[10];
};
int s;
struct sockaddr_ll sll;

int printbuf(unsigned char * b, int size){
	int i;
	for(i=0;i<size;i++)
		printf("%.2x(%.3d) ", b[i],b[i]);
	printf("\n");
}

int are_equal(void *a1, void *a2, int l)
{
char *b1 = (char *)a1;
char *b2 = (char *)a2;
for(int i=0; i<l; i++) if(b1[i]!=b2[i]) return 0;
return 1;
}

int resolve_mac(unsigned int ip, unsigned char * mac )
{
int i;
int len;
int k,n;
unsigned char buffer[1500];
struct ethernet_frame * eth;
struct arp_packet * arp; 
eth =(struct ethernet_frame *)  buffer;
arp = (struct arp_packet *) eth->payload;

arp->htype = htons(1) ;
arp->ptype  = htons(0x0800); 
arp->hlen = 6;
arp->plen = 4;
arp->op = htons(1);

for(i=0;i<6;i++) arp->srcmac[i]=mymac[i];
for(i=0;i<4;i++) arp->srcip[i]=myip[i];
for(i=0;i<6;i++) arp->dstmac[i]=0;
*(unsigned int*)arp->dstip = ip;

for(i=0;i<6;i++) eth->dstmac[i]=0xFF;
for(i=0;i<6;i++) eth->srcmac[i]=mymac[i];
eth->type = htons(0x0806);

printbuf(buffer,14+sizeof(struct arp_packet));
len = sizeof(struct sockaddr_ll);
bzero(&sll,len);
sll.sll_family = AF_PACKET;
sll.sll_ifindex = if_nametoindex("eth0");
n= sendto(s, buffer,14+sizeof(struct arp_packet),0, (struct sockaddr *)&sll ,len);
if(n == -1) { perror("ARP Req not sent"); return -1;} 

for(k=0;k<100;k++){
	n= recvfrom(s, buffer, 1500, 0,(struct sockaddr *) &sll, &len);
	if(ntohs(eth->type)==0x0806){
		if(are_equal(arp->srcip,&ip,4)){
				printf("ARP Reply:");
				printbuf(buffer,14+sizeof(struct arp_packet));
			for(i=0;i<6;i++) mac[i]=arp->srcmac[i];
			return 0;
			}
		}
	}
return -1;
}


int main(int argc, char * argv[])
{
int t,len;
unsigned char  inbuf[1500];
unsigned char targetip[4] = { 88,80,187,10 }; //Default if not specified as a parameter
unsigned char destmac[6];

if (argc == 2)
	*(unsigned int *)targetip=inet_addr(argv[1]);

printf("Target Address: %d.%d.%d.%d \n",targetip[0],targetip[1],targetip[2],targetip[3]);

s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
if ( s == -1 ){perror("Socket Failed"); return 1;}
t = resolve_mac(*(unsigned int *)targetip, destmac);
if ( t == -1 )
	 printf("Resolve error\n");
else { 
	printf("Resolved Mac: ");
	printbuf(destmac,6);
}
/*
len = sizeof(struct sockaddr_ll);
bzero(&sll,len);
sll.sll_family = AF_PACKET;
sll.sll_ifindex = if_nametoindex("eth0");
t= recvfrom(s, inbuf, 1500, 0,(struct sockaddr *) &sll, &len);
if (t==-1){perror("Recvfrom failed"); return 1;} 
printbuf(inbuf,t);
*/
} 
	

